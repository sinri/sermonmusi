<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/9/29
 * Time: 13:58
 */

require_once __DIR__ . '/../autoload.php';

//php /home/sinri/sermonmusi/bin/fuyin-tv.php mp3 '/home/sinri/movie' ''

if ($argc <= 3) {
    echo 'echo Usage:' . "php fuyin-tv.php [mp3] [url]" . PHP_EOL;
    exit(1);
}

$types = $argv[1];
$downloadDir = $argv[2];
$url = $argv[3];

if (false) {
    $types = "mp3";
    $downloadDir = __DIR__ . '/tmp';
    $url = "https://www.fuyin.tv/content/view/movid/2077/";
}

try {
    $crawler = new \sinri\sermonmusi\fuyintv\FuyinTVCrawler($url, $downloadDir);
    $crawler->downloadMp3();
} catch (Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
    echo $exception->getTraceAsString() . PHP_EOL;
}