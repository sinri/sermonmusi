<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/9/29
 * Time: 17:17
 */
require_once __DIR__ . '/../autoload.php';

$url = 'https://www.fuyin.tv/content/view/movid/2053/';
$downloadDir = __DIR__ . '/nosuch';

try {
    $crawler = new \sinri\sermonmusi\fuyintv\FuyinTVCrawler($url, $downloadDir);
    $crawler->setDryRun(true);
    $crawler->downloadMp3();
} catch (Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
    echo $exception->getTraceAsString() . PHP_EOL;
}