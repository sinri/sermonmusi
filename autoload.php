<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/9/29
 * Time: 13:55
 */

require_once __DIR__ . '/vendor/autoload.php';

\sinri\ark\core\ArkHelper::registerAutoload("sinri\sermonmusi", __DIR__ . '/src');