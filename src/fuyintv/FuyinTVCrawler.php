<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/9/29
 * Time: 13:47
 */

namespace sinri\sermonmusi\fuyintv;


use Psr\Log\LogLevel;
use sinri\ark\core\ArkHelper;
use sinri\ark\core\ArkLogger;
use sinri\ark\io\curl\ArkCurl;

class FuyinTVCrawler
{
    const TYPE_MP3 = "mp3";

    protected $url;
    protected $sourceCode;
    protected $downloadDir;
    protected $title;
    protected $logger;
    protected $dryRun;

    /**
     * FuyinTVCrawler constructor.
     * @param $url
     * @param $downloadDir
     * @throws \Exception
     */
    public function __construct($url, $downloadDir)
    {
        $this->dryRun = false;
        $this->logger = new ArkLogger();

        $this->url = $url;
        $this->downloadDir = $downloadDir;

        $this->downloadHTML();
        $this->fetchTitle();
    }

    /**
     * @throws \Exception
     */
    protected function downloadHTML()
    {
        $curl = new ArkCurl();
        $code = $curl->prepareToRequestURL("GET", $this->url)
            ->execute();
        ArkHelper::quickNotEmptyAssert("Cannot fetch available HTML PAGE SOURCE CODE", $code);
        $this->sourceCode = iconv("GBK", "UTF-8//IGNORE", $code);
    }

    protected function fetchTitle()
    {
        if (!preg_match('/<title>([^<]*)<\/title>/', $this->sourceCode, $matches)) {
            $this->title = md5($this->url);
        } else {
            $this->title = $matches[1];
        }
        $this->logger->info("Fetched Title: " . $this->title);
    }

    /**
     * @param bool $dryRun
     */
    public function setDryRun(bool $dryRun)
    {
        $this->dryRun = $dryRun;
        if ($this->dryRun) {
            $this->logger->setIgnoreLevel(LogLevel::DEBUG);
        }
    }

    /**
     * @param string[] $types
     */
    public function work($types)
    {
        if (in_array(self::TYPE_MP3, $types)) {
            $this->downloadMp3();
        }
    }

    public function downloadMp3()
    {
        $mp3Links = $this->fetchMP3Links();
        if ($this->dryRun) {
            $this->logger->warning("DRY RUN MODE");
        }
        $this->logger->info("Found MP3 Count:" . count($mp3Links));
        foreach ($mp3Links as $url) {
            $name = pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_BASENAME);
            $this->logger->debug("ORIGIN URL: " . $url);
            $this->logger->debug("PARSED NAME: " . $name);
            $cmd = "mkdir -p " . escapeshellarg($this->downloadDir . '/' . $this->title) . "; wget " . escapeshellarg($url) . ' -O ' . escapeshellarg($this->downloadDir . '/' . $this->title . "/" . $name);
            $this->logger->info("Ready to run: " . $cmd);
            $output = [];
            if (!$this->dryRun) exec($cmd, $output);
            foreach ($output as $line) {
                $this->logger->info("> " . $line);
            }
            $this->logger->info("Download One Part Over");
        }
        $this->logger->info("Finished downloading mp3");
    }

    protected function fetchMP3Links()
    {
        if (!preg_match_all('/href="(http[^"]+\.mp3)"/', $this->sourceCode, $matches)) {
            $this->logger->warning("Cannot find mp3 download link");
            return [];
        }
        return $matches[1];
    }
}