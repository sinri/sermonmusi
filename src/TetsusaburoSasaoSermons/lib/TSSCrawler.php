<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/10/15
 * Time: 22:31
 */

namespace sinri\sermonmusi\TetsusaburoSasaoSermons\lib;


use sinri\ark\core\ArkLogger;
use sinri\ark\io\curl\ArkCurl;

class TSSCrawler
{
    protected $logger;

    public function __construct()
    {
        $this->logger = new ArkLogger();
    }

    public function work()
    {
        for ($i = 1; $i <= 92; $i++) {
            try {
                $this->downloadOneSermon($i);
            } catch (\Exception $exception) {
                $this->logger->error("ERROR INDEX $i. " . $exception->getMessage());
            }
        }
    }

    /**
     * @param $index
     * @throws \Exception
     */
    public function downloadOneSermon($index)
    {
        $url = "http://www.geocities.co.jp/HeartLand-Namiki/3294/book31msg{$index}.html";
        $this->logger->info("url: " . $url);

        $curl = new ArkCurl();
        $curl->prepareToRequestURL("GET", $url);
        //$curl->setCURLOption(CURLOPT_PROXY,"127.0.0.1:1087");
        $text = $curl->execute();
        $text = iconv("Shift-JIS", "UTF-8//IGNORE", $text);

        $this->logger->info("text: ", [$text, $curl->getResponseCode(), $curl->getResponseMeta()]);

        if (!preg_match('/\<H3\>(.+)\<\/H3\>/', $text, $matches)) {
            throw new \Exception("Cannot find title");
        }
        $title = $matches[1];
        $this->logger->info("TITLE", [$title]);

        $markdown = "# " . ($index < 10 ? "0" : "") . $index . " " . $title . PHP_EOL . PHP_EOL;

        $matches = [];
        preg_match_all('/\<P CLASS="(TEXT|PARAGRAPH)"\>([^<]+)\<\/P\>/', $text, $matches);
        foreach ($matches[2] as $match) {
            $markdown .= $match . PHP_EOL . PHP_EOL;
        }

        $file = __DIR__ . '/../../../data/TetsusaburoSasaoSermons';
        if (!file_exists($file)) {
            mkdir($file, 0777, true);
        }
        $file .= '/' . ($index < 10 ? "0" : "") . $index . ".md";
        $written = file_put_contents($file, $markdown);
        $this->logger->info("Written", [$written]);
    }
}